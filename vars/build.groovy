def call(String repoUrl){
pipeline {
    agent any

    stages {
        stage('checkout') {
            steps {
            git 'https://github.com/opstree/OT-Microservices.git'
            }
        }
    
          stage('Code Compilation') {
            steps {
            dir ('/var/lib/jenkins/workspace/golangci/employee') {
            sh ('go build employee_api.go')
            }
        }
          }
          
           stage('Static code analysis') {
            steps {
            dir ('/var/lib/jenkins/workspace/golangci/employee') {
            sh ('golint employee_api.go ')
            }
        }
          }
            stage('Bug Analysis') {
            steps {
            dir ('/var/lib/jenkins/workspace/golangci/employee') {
            sh ('go bug ')
            }
        }
          }
           stage('Unit Testing') {
            steps {
            dir ('/var/lib/jenkins/workspace/golangci/employee') {
            sh ('go test ')
            }
        }
          }
          
           stage('Dependency Scanning') {
            steps {
            dir ('/var/lib/jenkins/workspace/golangci/employee') {
            sh ('go mod verify ')
            }
        }
          }
    }
    
}

}

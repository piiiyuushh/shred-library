module employee

go 1.18

require (
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20200630125629-8413c97f3011
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	go.elastic.co/apm/module/apmelasticsearch/v2 v2.1.0
	go.elastic.co/apm/module/apmgin/v2 v2.1.0
	gopkg.in/yaml.v2 v2.2.8
)

require (
	github.com/cweill/gotests v1.6.0 // indirect
	github.com/rs/xid v1.4.0
)
